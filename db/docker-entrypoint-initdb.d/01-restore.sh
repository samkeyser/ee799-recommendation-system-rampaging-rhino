#!/usr/bin/env bash
# Based on work documented in second comment (Petr Ujezdsky) here:
# https://stackoverflow.com/questions/29600369/starting-and-populating-a-postgres-container-in-docker

file="/dump/dump.pgdata"
dbname="ydata"

echo "Restoring ${dbname} using ${file}"
createdb -e ${dbname}
time pg_restore -U postgres --dbname=${dbname} --verbose --single-transaction < "${file}" || exit $?
echo "Done restoring"
