import os
import pickle
from minio import Minio
from minio.error import S3Error

class MinioAPI:
    def __init__(self):
        minio_user = None
        with open(os.environ.get("MINIO_ROOT_USER_FILE"), 'r') as datafile:
            minio_user = datafile.readline().strip()

        minio_password = None
        with open(os.environ.get("MINIO_ROOT_PASSWORD_FILE"), 'r') as datafile:
            minio_password = datafile.readline().strip()


        minio_host = os.environ.get("MINIO_URL") + ":" + os.environ.get("MINIO_PORT")
        self.client = Minio(minio_host,
                            minio_user,
                            minio_password,
                            secure=False)

    def fput(self, bucket, object_name, filepath):
        if not self.client.bucket_exists(bucket):
            self.client.make_bucket(bucket)

        self.client.fput_object(bucket, object_name, filepath)

    def fget(self, bucket, object_name, destination):
        assert self.client.bucket_exists(bucket), f"{bucket} does not exist"

        self.client.fget_object(bucket, object_name, destination)

def get_data():
    # This should return a scipy csr_matrix
    print("Reading environment variables", flush=True)
    data_pickle = os.environ.get("SPARSE_MAT_OBJ", "sparse_matrix.pickle")

    print('Creating minio api object', flush=True)
    minio = MinioAPI()

    print('Pulling', data_pickle, flush=True)
    minio.fget("model-data", data_pickle, data_pickle)

    print('Unpickling data')
    sparse_mat = None
    with open(data_pickle, 'rb') as datafile:
        sparse_mat = pickle.load(datafile)

    return sparse_mat

def push_data(filepath):
    # This should take a path to a local file and then push it up to minio
    print("Reading environment variables", flush=True)
    data_pickle = os.environ["SPARSE_MAT_OBJ"]

    print('Creating minio api object', flush=True)
    minio = MinioAPI()

    print("Pushing", filepath, "to Minio as", filepath, flush=True)
    minio.fput("model-files", filepath, filepath)
