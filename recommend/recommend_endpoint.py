import numpy as np
import psycopg2
from minio import Minio
from minio.error import S3Error
# using flask_restful
from scipy.sparse import csr_matrix
from flask import Flask, jsonify, request
from flask_restful import Resource, Api
import json
import pickle
from datetime import datetime
import os

from models import SVDModel, AverageRatingModel

# Globals
svd_model = None
avg_ratings_model = None

def get_secret(secret):
    s = os.getenv('secret')
    if not s:
        with open(os.getenv((secret+"_FILE")), "r") as f:
            s = f.readline().strip()
    return s

def get_db_connection():
    conn = psycopg2.connect(
        database = os.getenv('POSTGRES_DATABASE'),
        user = get_secret('POSTGRES_USER'),
        password = get_secret('POSTGRES_PASSWORD'),
        host = os.getenv('POSTGRES_HOST'),
        port = os.getenv('POSTGRES_PORT'))

    conn.set_client_encoding('UTF8')

    return conn

class MinioAPI:
    def __init__(self):
        minio_user = None
        with open(os.environ.get("MINIO_ROOT_USER_FILE"), 'r') as datafile:
            minio_user = datafile.readline().strip()

        minio_password = None
        with open(os.environ.get("MINIO_ROOT_PASSWORD_FILE"), 'r') as datafile:
            minio_password = datafile.readline().strip()


        print(os.environ)
        minio_host = os.environ.get("MINIO_URL") + ":" + os.environ.get("MINIO_PORT")
        self.client = Minio(minio_host,
                            minio_user,
                            minio_password,
                            secure=False)

    def fput(self, bucket, object_name, filepath):
        if not self.client.bucket_exists(bucket):
            self.client.make_bucket(bucket)

        self.client.fput_object(bucket, object_name, filepath)

    def fget(self, bucket, object_name, destination):
        assert self.client.bucket_exists(bucket), f"{bucket} does not exist"

        self.client.fget_object(bucket, object_name, destination)

# creating the flask app
app = Flask(__name__)
# creating an API object
api = Api(app)

# Global variables
THRESHOLD = int(os.environ.get("COLD_START_THRESHOLD", 20))

# Class that represents the /recommend object
class Recommend(Resource):

    # Corresponds to POST request
    def get(self):
        BATCH_SIZE = int(os.environ.get("BATCH_SIZE", 1024))
        TOP_K = int(os.environ.get("TOP_K", 5))
        N_MOST_RECENT = int(os.environ.get("N_MOST_RECENT"))

        data = request.get_json()

        # Pull the user vector
        cols = []
        values = []
        items = 0
        with get_db_connection() as conn:
            # TODO: this may need to be batched as a server side cursor (see process_data.py)
            with conn.cursor('server-side-recommend-endpoint-cursor') as cur:
                cur.itersize = BATCH_SIZE
                cur.execute('SELECT song_id,rating FROM ratings WHERE user_id = (%s) ORDER BY timestamp ASC;', [str(data['user_id'])])

                # Pull the data down in batches
                batch = cur.fetchmany(BATCH_SIZE)
                while len(batch) > 0:
                    for song, rating in batch:
                        cols.append(int(song))
                        values.append(int(rating))
                    batch = cur.fetchmany(BATCH_SIZE)

            with conn.cursor('server-side-recommend-endpoint-cursor-query-items') as cur:
                cur.execute('SELECT DISTINCT MAX(song_id) from ratings;')
                items = int(cur.fetchone()[0])+1
        cols = np.array(cols)
        values = np.array(values)
        user_vec = csr_matrix((values, (np.zeros(cols.shape), cols)), shape=(1,items))

        # Select the model which we want to use to server the user
        ## By default we use avg_rating model, or the svd model if a user has at least some umbe rof entries in the db
        model = svd_model if len(values) > THRESHOLD else avg_ratings_model

        # Get the predictions
        pred = model.predict(user_vec)

        # Filter out things we've listened to too recently by setting them to zero
        recently_listened_to = cols[:N_MOST_RECENT]
        pred[0,recently_listened_to] = 0

        top_k = data['total_recommendations'] if data['total_recommendations'] else 10

        # Take the top k things
        pred_song_ids = list(np.flip(pred.indices[pred.data.argsort()[-top_k:]]))
        pred_song_ids = list(map(int, pred_song_ids))

        return { 'recommendations': pred_song_ids }, 200

# Adding the defined resources along with their corresponding urls
api.add_resource(Recommend, '/recommend')

# driver function
if __name__ == '__main__':
    minio = MinioAPI()
    # Pull down the pickles only if the bucket is in minio (implies that the model objects also exist)
    if minio.client.bucket_exists("model-files"):
        minio.fget("model-files", "svd_model.pickle", "svd_model.pickle")
        minio.fget("model-files", "avg_rating_model.pickle", "avg_rating_model.pickle")

    with open("svd_model.pickle", 'rb') as datafile:
        svd_model = pickle.load(datafile)

    with open("avg_rating_model.pickle", 'rb') as datafile:
        avg_ratings_model = pickle.load(datafile)

    app.run(debug = False, host='0.0.0.0', port = 8889 )
