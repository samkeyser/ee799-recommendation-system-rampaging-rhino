#!/usr/bin/env bash
# Utility script for creating binary db dump, based on work documented in second comment (Petr Ujezdsky) here:
# https://stackoverflow.com/questions/29600369/starting-and-populating-a-postgres-container-in-docker

USERNAME=postgres
CONTAINER=rampaging-rhino-db-1
DB=ydata

while [[ $# -gt 0 ]]
do
    case $1 in
        "-u" | "--username")
            shift 1
            USERNAME=$1
            ;;
        "-d" | "--database")
            shift 1
            DB=$1
            ;;
        "-c" | "--container")
            shift 1
            CONTAINER=$1
            ;;
        *)
            echo "Unknown argument " $1
            echo "Exiting..."
            exit 1
            ;;
    esac
    shift 1
done

docker exec "${CONTAINER}" pg_dump -U"${USERNAME}" --format custom "${DB}" > "dump.pgdata"
